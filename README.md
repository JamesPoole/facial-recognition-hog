# Facial Recognition using HOG Techniques

# Prerequisites

The lfw dataset - http://vis-www.cs.umass.edu/lfw/lfw.tgz

Python Packages Required: (Not complete list and needs to be updated)
```
numpy
scipy
opencv-python
matplotlib
sklearn
```

# Setup

In file data_split.py, function get_train_test_set(), change the path to the lfw dataset to match your path

# Info

Train time - ~20mins on Intel i5-4690
Accuracy = .0088 (Work in progress :) )
