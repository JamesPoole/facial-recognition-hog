import cv2
import numpy as np
from matplotlib import pyplot as plt
from sklearn import svm as sk_svm

#use facenet functions to quickly parse through dataset
import data_parse
import data_split

#values and setup for the HOG Descriptor
win_size = (250, 250)
block_size = (10, 10)
cell_size = (5, 5)
block_stride = (5, 5)
nbins = 9
deriv_aperture = 1
win_sigma = 4.
histogram_norm_type = 0
l2_hys_threshold = 2.0000000000000001e-01
gamma_correction = 0
nlevels = 64

hog = cv2.HOGDescriptor(win_size,block_size,block_stride,cell_size,nbins,deriv_aperture,win_sigma,
                                histogram_norm_type,l2_hys_threshold,gamma_correction,nlevels)

"""
get_hog - function to take in a train or test set and find the hog for each image

args    class_set - set to scan for images and find hogs of

return  data - array of hogs    
        labels - labels to match the data
"""
def get_hog(class_set):
    data = []
    labels = []
    for cls in class_set:
        for image in cls.image_paths:
            img = cv2.imread(image)

            descriptor = hog.compute(img)

            #add data and label to respective arrays
            data.append(descriptor)
            labels.append(cls.name)

    #convert list to numpy array for compatibility with the svm
    data = np.asarray(data)
    labels = np.asarray(labels)

    #return the array of hogs and labels
    return data, labels

"""
labels_to_int - function to convert labels to ints because svm will not accept strings as labels

args    labels - array of labels to be converted

returns int_labels - array of labels in int form 
        int_label_dict - dictionary for easy lookup of name to int
"""
def labels_to_int(labels):
    current_name = ""
    current_int = 0
    int_labels = []
    int_label_dict ={}
    for name in labels:
        if name != current_name:
            current_name = name
            int_labels.append(current_int)
            int_label_dict.update({current_name: current_int})
            current_int += 1
        else:
            int_labels.append(current_int)
    
    int_labels = np.asarray(int_labels)

    #returns the label array in ints and a dictionary to easily look up what number belongs to what person
    return int_labels, int_label_dict

"""
int_label_lookup -  function to convert test labels to ints.
                    when given a test set, the data is randomised for fairness
                    but we need to make sure to match the correct labels to the correct ints.

args    test_labels - labels for use in test
        int_label_dict - dictionary for easy label int lookup

return  int_labels - test labels converted to ints
"""
def int_label_lookup(test_labels, int_label_dict):
    int_labels = []
    for name in test_labels:
        int_labels.append(int_label_dict[name])  

    return int_labels

"""
data_reshape - function to reshape the data to work with libsvm

args    data - dataset to reshape

return reshaped data - reshaped dataset
"""
def data_reshape(data):
    data_size = len(data)
    reshaped_data = data.reshape(data_size, -1)

    return reshaped_data

"""
train_svm - function to run the training on the svm

args    data - data set for training (in numpy array form)
        int_labels - labels for the data set in int form

returns svm - svm to test and run
"""
def train_svm(data, int_labels):
    #set up svm
    svm = sk_svm.SVC()
    print("Starting to train svm...")
    #train svm
    svm.fit(data, int_labels)
    print("SVM training finished...")

    return svm

"""
test_svm - function to test the trained svm

args    svm - trained svm
        test_data - test data set

returns test_response
"""
def test_svm(svm, test_data):
    print("Starting to test svm...")
    test_response = svm.predict(test_data)
    return test_response

"""
check_accuracy - function to check the accuracy of the test responses

args    labels - original array of labels in int form
        predicts - array of predicted labels

returns accuracy - percentage accuracy
"""
def check_accuracy(labels, predicts):
    success_check = labels == predicts

    success = 0
    for result in success_check:
        if result == True:
            success += 1

    accuracy = float(success) / len(labels)

    return accuracy


"""
1) get train and test sets
2) find the hog and labels of each image
3) convert labels to ints
4) reshape train data array
5) train svm using train data and int labels
"""
train_set, test_set = data_split.get_train_test_set()
train_data, train_labels = get_hog(train_set)
int_train_labels, int_label_lookup_dict = labels_to_int(train_labels)
reshaped_train_data = data_reshape(train_data)
svm = train_svm(reshaped_train_data, int_train_labels)

"""
1) find the hog and labels of each test image
2) convert test labels to ints
3) run the test svm
"""
test_data, test_labels = get_hog(test_set)
int_test_labels = int_label_lookup(test_labels, int_label_lookup_dict)
reshaped_test_data = data_reshape(test_data)
test_response = test_svm(svm, reshaped_test_data)
accuracy = check_accuracy(int_test_labels, test_response)
print(accuracy)
